import logging
from pathlib import Path
from shutil import copy

from localconfig import LocalConfig
from torch import multiprocessing as mp

from decentralizepy import utils
from decentralizepy.graphs.Graph import Graph
from decentralizepy.mappings.Linear import Linear
from decentralizepy.node.DPSGDNode import DPSGDNode


def read_ini(file_path):
    config = LocalConfig(file_path)
    for section in config:
        print("Section: ", section)
        for key, value in config.items(section):
            print((key, value))
    print(dict(config.items("DATASET")))
    return config # return a localConfig object

if __name__ == "__main__":
    args = utils.get_args()

    Path(args.log_dir).mkdir(parents=True, exist_ok=True)

    #Create a dict
    #The script creates a dictionary log_level that maps log level names 
    # to their corresponding integer values.
    log_level = {
        "INFO": logging.INFO,
        "DEBUG": logging.DEBUG,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
        "CRITICAL": logging.CRITICAL,
    }

    #Create a dict
    #Read the config options - stores them in a dict
    config = read_ini(args.config_file)
    my_config = dict()
    for section in config:
        my_config[section] = dict(config.items(section))

    copy(args.config_file, args.log_dir)
    copy(args.graph_file, args.log_dir)
    utils.write_args(args, args.log_dir)

    # Create a graph object
    g = Graph()
    g.read_graph_from_file(args.graph_file, args.graph_type)
    n_machines = args.machines
    procs_per_machine = args.procs_per_machine[0]

    # Cretes object Linear: a mapping given the n of machines 
    # and n of processes by machine
    l = Linear(n_machines, procs_per_machine)
    m_id = args.machine_id

    # create a list to store mp.Process objects for each procs per machine
    processes = []

   
    # The 'Process' class is used to spawn(lancer) a new process
    for r in range(procs_per_machine):
        processes.append(
            mp.Process(
                target=DPSGDNode,
                args=[
                    r,
                    m_id,
                    l, #mapping: linear
                    g, # Graph object
                    my_config,
                    args.iterations,
                    args.log_dir,
                    args.weights_store_dir,
                    log_level[args.log_level],
                    args.test_after,
                    args.train_evaluate_after, # Takes the same value as test_after here (20) see the .sh
                    args.reset_optimizer,
                ],
            )
        )

    #Each process: we create a new DPSGDNode (by calling the __init__())
    #and the we call the run() method
    for p in processes:
        p.start()
    

    for p in processes:
        p.join()
