import importlib
import logging
import numpy as np
import torch


CHUNK_SIZE = 4000
class Sharing:
    """
    API defining who to share with and what, and what to do on receiving

    """

    def __init__(
        self,
        rank,
        machine_id,
        communication,
        mapping,
        graph,
        model,
        dataset,
        log_dir,
        compress=False,
        compression_package=None,
        compression_class=None,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id 
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yet! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        self.rank = rank
        self.machine_id = machine_id
        self.uid = mapping.get_uid(rank, machine_id)
        self.communication = communication
        self.mapping = mapping
        self.graph = graph
        self.model = model
        self.dataset = dataset
        self.communication_round = 0
        self.log_dir = log_dir

        self.shapes = [] # store shapes of each layer
        self.lens = [] # store len() of each layer
        with torch.no_grad():
            for _, v in self.model.state_dict().items(): # (k,v) : 'fc1.weights' <- tensor([],[])
                self.shapes.append(v.shape) # stores shape
                t = v.flatten().numpy()
                self.lens.append(t.shape[0]) # stores the number of weights in 1 layer. Number of component after flatening.

        self.compress = compress

        if compression_package and compression_class:
            compressor_module = importlib.import_module(compression_package)
            compressor_class = getattr(compressor_module, compression_class)
            self.compressor = compressor_class()
            logging.debug(f"Using the {compressor_class} to compress the data")
        else:
            assert not self.compress

    def compress_data(self, data):
        result = dict(data)
        #compress arg is often 'False'
        if self.compress:
            if "params" in result:
                result["params"] = self.compressor.compress_float(result["params"])
        return result

    def decompress_data(self, data):
        if self.compress:
            if "params" in data:
                data["params"] = self.compressor.decompress_float(data["params"])
        return data

    def serialized_model(self):
        """
        Convert model to a dictionary. Here we can choose how much to share in the SubSampling module

        Returns
        -------
        dict
            Model converted to dict

        """
        to_cat = []
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                t = v.flatten()
                to_cat.append(t)
        flat = torch.cat(to_cat)
        data = dict()
        data["params"] = flat.numpy()
        return self.compress_data(data)
    


    def deserialized_model(self, m):
        """
        Convert received dict to state_dict.

        Parameters
        ----------
        m : dict (serialized)
            received dict

        Returns
        -------
        state_dict
            state_dict of received

        """
        state_dict = dict()
        m = self.decompress_data(m)
        T = m["params"]
        start_index = 0
        for i, key in enumerate(self.model.state_dict()): # Will only iterate on the keys of the local pytorch model.state_dict()
            end_index = start_index + self.lens[i]
            #Creates a Tensor from a numpy.ndarray
            state_dict[key] = torch.from_numpy(
                T[start_index:end_index].reshape(self.shapes[i])
            )
            start_index = end_index

        return state_dict

    def _pre_step(self):
        """
        Called at the beginning of step.

        """
        pass

    def _post_step(self):
        """
        Called at the end of step.

        """
        pass

    def _averaging(self, peer_deques):
        """
        Averages the received model with the local model
        peer_deques: dict of deque() {'sender1': deque() , 'sender2': deque()}
        """
        with torch.no_grad():
            total = dict() #sum of model params
            weight_total = 0 #their weights
            for i, n in enumerate(peer_deques):
                if len(peer_deques[n]) > 0 :
                    data = peer_deques[n].popleft()
                    degree, iteration = data["degree"], data["iteration"] #from my neighbour
                    del data["degree"]
                    del data["iteration"]
                    del data["CHANNEL"]
                    logging.debug(
                        "Averaging model from neighbor {} of iteration {}".format(
                            n, iteration
                        )
                    )
                    data = self.deserialized_model(data) #deserializes the model update of my neighbour 
                    weight = 1 / (max(len(peer_deques), degree) + 1)
                    weight_total += weight
                    for key, value in data.items(): 
                        if key in total:
                            total[key] += value * weight
                        else:
                            total[key] = value * weight

            for key, value in self.model.state_dict().items(): #update current model
                total[key] += (1 - weight_total) * value  # Metro-Hastings

        self.model.load_state_dict(total)
        self._post_step()
        self.communication_round += 1

    def get_data_to_send(self, degree=None):
        self._pre_step()
        data = self.serialized_model() 
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        data["degree"] = degree if degree != None else len(self.graph.neighbors(my_uid))
        data["iteration"] = self.communication_round
        return data
    
    
    def get_data_to_send_UDP(self,ite, degree=None):
        """
        Prepare the data to be sent

        Parameters
        ----------
        ite : int
            Current iteration
        degree: int
            Degree of the sending node (his number of neighbors)

        Returns
        -------
        data : dict
            returns the dict data with 3 keys: "params", "degree" and "iteration"
        
        """ 
        self._pre_step()
        data = self.serialized_model_UDP() # Dict with only 1 key : "params"
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        data["degree"] = degree if degree != None else len(self.graph.neighbors(my_uid)) #add 2nd key
        data["iteration"] = ite #add 3rd key
        return data
    
    def serialized_model_UDP(self):
        """
        Serialize the model

        Returns
        -------
        data : dict
            dict with one key "params" containing flattened model

        """
        to_cat = [] #Here we store the 1D arrays for each layer,  need to concatenate them to make it an all  1D tensor 
        with torch.no_grad():
            for _, v in self.model.state_dict().items():
                t = v.flatten() # t is a 1D tensor
                to_cat.append(t)
        flat = torch.cat(to_cat)
        data = dict()
        data["params"] = flat.numpy() #NumPy array
        return data

    def _averaging_server(self, peer_deques):

        """
        Averages the received models of all working nodes

        """
        with torch.no_grad():
            total = dict()
            for i, n in enumerate(peer_deques):
                data = peer_deques[n].popleft()
                degree, iteration = data["degree"], data["iteration"]
                del data["degree"]
                del data["iteration"]
                del data["CHANNEL"]
                logging.debug(
                    "Averaging model from neighbor {} of iteration {}".format(
                        n, iteration
                    )
                )
                data = self.deserialized_model(data)
                weight = 1 / len(peer_deques)
                for key, value in data.items():
                    if key in total:
                        total[key] += weight * value
                    else:
                        total[key] = weight * value

        self.model.load_state_dict(total)
        self._post_step()
        self.communication_round += 1
        return total
    

    def _averaging_UDP(self, received_models ,curr_iteration):
        """
        Averages the received models with the local model

        Parameters
        ----------
        received_models : nested dict
            where received parameters from neighbors are stored
        curr_iteration : int 
            current iteration

        """

        if received_models :
            if received_models[curr_iteration]:
                with torch.no_grad():
                    total = dict() #sum of model params
                    weight_total = 0 #their weights

                    for i, uid_sender in enumerate(received_models[curr_iteration]):
                        if len(received_models[curr_iteration][uid_sender]) > 0 :
                            data = received_models[curr_iteration][uid_sender] # 2nd depth of the nested dict
                            degree, iteration = data["degree"], data["iteration"] #from my neighbour
                            del data["degree"]
                            del data["iteration"]
                            logging.info(
                                "Averaging model from neighbor {} of iteration {}".format(
                                    uid_sender, iteration
                                )
                            )

                            merged_data = self.deserialized_model_UDP(data) #returns state_dict : DESERIALIZE + merges the model update of my neighbour 
                            #It returns the 'state_dict of received' where missing values from the neighbor are remplaced by local updates

                            weight = 1 / (max(len(received_models[curr_iteration]), degree) + 1)
                           
                            weight_total += weight
                            for key, value in merged_data.items(): # Go threw the keys of data - that is the name of each layer 
                                if key in total:
                                    total[key] += value * weight
                                else:
                                    total[key] = value * weight
                    for key, value in self.model.state_dict().items(): #update current model within the self.state_dict
                        total[key] += (1 - weight_total) * value  # Metro-Hastings

                self.model.load_state_dict(total)
                self._post_step()
                self.communication_round += 1


        

    def deserialized_model_UDP(self, m):
        """
        Deserialize received model

        Parameters
        ----------
        m : dict
            Dictionary where keys are the index of the chunks and the value is the chunk of parameter

        Returns
        -------
        state_dict : dict
            The keys of this dictionary are the names of the model’s parameters, and the values are the corresponding parameter tensors. 
            (where missing values from the neighbor are remplaced by local updates)
        
        """         
        with torch.no_grad():
            state_dict = self.model.state_dict()
            shapes = []
            lens = []
            tensors_to_cat = []
            for _, v in state_dict.items():
                shapes.append(v.shape)
                t = v.flatten()
                lens.append(t.shape[0])
                tensors_to_cat.append(t)

            T = torch.cat(tensors_to_cat, dim=0).numpy() #local
            chunks = [T[i:i+CHUNK_SIZE] for i in range(0, len(T), CHUNK_SIZE)] #Serialize the local params  into list of numpy arrays 
            temp_dict = {}

            index = 0
            for c in chunks : 
                temp_dict[index] = c
                index +=1
            
            merged_flattened = [] #need to concatenate this and reshape it 

            ## Merge with received
            for k,v in temp_dict.items(): # k is the index of the chunks, v is the chunks of param
                if k in m: #if received from neighbor
                    merged_flattened.append(m[k])
                else: #otherwise replace with local param
                    merged_flattened.append(v)  
            
            merged_flattened_concatenated = torch.from_numpy(np.concatenate(merged_flattened)) #local merged with received -> to reconstruct
            start_index = 0
            for i, key in enumerate(state_dict):
                end_index = start_index + lens[i]
                state_dict[key] = merged_flattened_concatenated[start_index:end_index].reshape(shapes[i])
                start_index = end_index
            return state_dict

