
from decentralizepy.communication.Communication import Communication
import json
import logging
import pickle
from collections import deque
from time import sleep
import socket
import threading 
import random 
import time
import gzip
from collections import defaultdict
from time import perf_counter
import torch

from collections import defaultdict

import zmq
CHUNK_SIZE = 4000 # Number of parameters sent per packet

class UDP(Communication):

    def get_IP_and_port(self, rank, machine_id):
        """
        Returns the pair 

        Parameters
        ----------
        rank : int
            Local rank of the process
        machine_id : int
            Machine id of the process

        Returns
        -------
        Tuple :
            machine_addr(IP), port

        """
        machine_addr = self.ip_addrs[str(machine_id)]
        port = (2 * rank + 1) + self.offset
        assert port > 0
        return machine_addr, port #host, port
    

    def __init__(
        self,
        rank,
        machine_id,
        mapping,
        total_procs,
        addresses_filepath,
        offset=9000,
        recv_timeout=50,
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank of the process
        machine_id : int
            Machine id of the process
        mapping : decentralizepy.mappings.Mapping
            uid, rank, machine_id invertible mapping
        total_procs : int
            Total number of processes
        addresses_filepath : str
            JSON file with machine_id -> ip mapping

        """
        super().__init__(rank, machine_id, mapping, total_procs)

        with open(addresses_filepath) as addrs:
            self.ip_addrs = json.load(addrs)

        self.total_procs = total_procs
        self.rank = rank
        self.machine_id = machine_id
        self.mapping = mapping
        self.offset = offset
        self.recv_timeout = recv_timeout
        self.uid = mapping.get_uid(rank, machine_id)
        self.identity = str(self.uid).encode()

        self.total_data = 0
        self.total_meta = 0

        host, port = self.get_IP_and_port(self.rank, self.machine_id)
        self.host = host #IP
        self.port = port

        
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) #UDP socket
        self.socket.bind((host, port)) #bind socket to the node to receive messages

        self.log = defaultdict(list)

    def send(self, neighbor, data_to_send):
        # calls send_chunks to chunk the data to send
        self.send_chunks(neighbor, data_to_send)


    def send_chunks(self, neighbor ,data_to_send):
        """
        Chunks and send the data to the given neighbor 

        Parameters
        ----------
        neighbor : int
            uid of the neighbor
        data_to_send : dict
            Dict with 3 keys: Params, iteration, degree
            and data_to_send["params"] is the data that is serialized to 1D Numpy array

        """
        dest_host, dest_port = self.get_IP_and_port(*self.mapping.get_machine_and_rank(neighbor))
        flattened = data_to_send["params"] # The params in a 1D Numpy array
        degree = data_to_send["degree"]
        iteration = data_to_send["iteration"]

        chunks = [flattened[i:i+CHUNK_SIZE] for i in range(0, len(flattened), CHUNK_SIZE)] # a list of numpy arrays
        
        index = 0
        for c in chunks:
            to_send = dict()
            to_send["index"] = index 
            to_send["param_chunk"] = c #params
            to_send["uid_sender"] = self.uid
            to_send["degree"] = degree
            to_send["iteration"] = iteration
            

            to_send_bytes = pickle.dumps(to_send)
    
            bytes_sent = self.socket.sendto(to_send_bytes, (dest_host, dest_port)) #send to dest node
            logging.info(
                                    "I am {} sending right now to neighbor {} of iteration {} chunk index {}".format(
                                        self.uid, neighbor , iteration, index
                                    )
                                )
            self.total_bytes += bytes_sent
            if bytes_sent != 0 : #successfully sent
                self.log[iteration].append(bytes_sent)

            index += 1




    def receive_packet(self):
        """
        Captures 1 packet 
        """ 
        try:
            self.socket.settimeout(0.1)  
            data, addr = self.socket.recvfrom(16250) 
            return data, addr[0], addr[1] #return data, IP_ofSender, port_ofSender
        except socket.timeout: 
            return None
        
    def receive(self,curr_iter,received_models):
        """
        Captures and stores the received packets 

        Parameters
        ----------
        curr_iter[0] : int
            current iteration
        received_models : Nested Dict to store parameters 
        """ 
        timeout = 0.25  # Maximum time to wait for messages (in seconds)
        start_time = perf_counter() # Get the current time
        
        while  (perf_counter() - start_time < timeout):
            received_data = self.receive_packet()
            if received_data: 
                data_bytes, IP_ofSender , port_ofSender = received_data
                data = pickle.loads(data_bytes) # Data that you received is a dict with 5 keys : The chunk, its idx, uid_sender, degree , iteration
                degree = data["degree"]
                iteration = data["iteration"]
                param_chunk = data["param_chunk"] #one layer param
                index = data["index"]
                uid_sender = data["uid_sender"]
                logging.info(
                                "I am {} receiving right now from neighbor {} of iteration {} chunk index {}".format(
                                    self.uid, uid_sender, iteration, index
                                )
                            )
                
                # Drop old packets 
                if iteration >= curr_iter[0]:
                    received_models[iteration][uid_sender]["degree"] = degree
                    received_models[iteration][uid_sender]["iteration"] = iteration
                    received_models[iteration][uid_sender][index] = param_chunk  


    def close_socket(self):
        self.socket.close()
    

    



